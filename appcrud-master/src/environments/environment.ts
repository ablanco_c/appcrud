// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
      apiKey: "AIzaSyA3pBopepMrybG8hvrkdOYsrxSY0-aGiKY",
      authDomain: "app2-41220.firebaseapp.com",
      databaseURL: "https://app2-41220.firebaseio.com",
      projectId: "app2-41220",
      storageBucket: "app2-41220.appspot.com",
      messagingSenderId: "194773917877",
      appId: "1:194773917877:web:9f072308ec64dd79a2defc"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
